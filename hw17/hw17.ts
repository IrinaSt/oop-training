// Задача 17.1: Сделайте класс User с публичным свойствами name (имя) и surname (фамилия).
// Задача 17.2: Сделайте класс Employee, который будет наследовать от класса User и добавлять salary (зарплата).
// Задача 17.3: Сделайте класс City с публичными свойствами name (название города) и population (количество населения).
// Задача 17.4: Создайте 3 объекта класса User, 3 объекта класса Employee, 3 объекта класса City, и в произвольном порядке запишите их в массив $arr.
// Задача 17.5: Переберите циклом массив $arr и выведите на экран столбец свойств name тех объектов, которые принадлежат классу User или потомку этого класса.
// Задача 17.6: Переберите циклом массив $arr и выведите на экран столбец свойств name тех объектов, которые НЕ принадлежат классу User или потомку этого класса.
// Задача 17.7: Переберите циклом массив $arr и выведите на экран столбец свойств name тех объектов, которые принадлежат именно классу User, то есть не классу City и не классу Employee.

class User {
  public name: string;
  public surname: string;

  constructor(name: string, surname: string) {
    this.name = name;
    this.surname = surname;
  }
}

class Employee extends User {
  public salary: number;

  constructor(name: string, surname: string, salary: number) {
    super(name, surname);
    this.salary = salary;
  }

  public __proto__: Object;
}

class City {
  public name: string;
  public population: number;

  constructor(name: string, population: number) {
    this.name = name;
    this.population = population;
  }
}

const user1 = new User("Irina", "St");
const user2 = new User("Anna", "St");
const user3 = new User("Inna", "Sti");

const employee1 = new Employee("Ivan", "Mo", 1000);
const employee2 = new Employee("Dmytro", "Me", 2000);
const employee3 = new Employee("Petro", "Mi", 3000);

const city1 = new City("Kiev", 30000);
const city2 = new City("Boston", 40000);
const city3 = new City("New-York", 10000);

const $arr = [
  user1,
  user2,
  user3,
  employee1,
  employee2,
  employee3,
  city1,
  city2,
  city3,
];

// only user and inherited from user
$arr
  .filter((element) => element instanceof User)
  .forEach((element) => {
    console.log(element.name);
  });

// not User
$arr
  .filter((element) => !(element instanceof User))
  .forEach((element) => {
    console.log(element.name);
  });

// only User
$arr
  .filter(function (element) {
    return element.constructor["name"] === "User";
  })
  .forEach(function (element) {
    console.log(element.name);
  });
