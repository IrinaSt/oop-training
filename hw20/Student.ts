interface CoureAdministrator {
  name: string;
  surname: string;
  patronymic: string;
}

export default class Student {
  private $name: string;
  private $surname: string;
  private $course: number;
  private $courseAdministrator: CoureAdministrator;

  constructor(name: string, course: number) {
    this.$name = name;
    this.$course = course;
  }

  public transferToNextCourse(): void {
    if (this.isCourseCorrect()) {
      this.$course = ++this.$course;
    }
  }

  private isCourseCorrect(): boolean {
    return this.$course < 5;
  }

  public setCourseAdministrator(newData: CoureAdministrator): void {
    if (this.isCorrectCourseAdministratorValues()) {
      this.$courseAdministrator = newData;
    }
  }

  public getCourseAdministrator(): CoureAdministrator {
    return this.$courseAdministrator;
  }

  protected isCorrectCourseAdministratorValues(): boolean {
    if (!this.$courseAdministrator) {
      return false;
    }
    if (!this.$courseAdministrator.name) {
      return false;
    }
    if (!this.$courseAdministrator.surname) {
      return false;
    }

    return true;
  }
}
