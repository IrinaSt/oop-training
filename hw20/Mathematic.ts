import Student from './Student';

export default class Mathematic extends Student {
    private iq: number;

    constructor(name: string, course: number, iq: number) {
         super(name, course);
         this.iq = iq;
    }
    isCorrectCourseAdministratorValues(): boolean {
        if(super.isCorrectCourseAdministratorValues() && this.getCourseAdministrator().patronymic){
           return true;
        }
        return false;
      
    }
}