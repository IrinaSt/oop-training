// Задача 18.1: Сделайте класс Post (должность), в котором будут следующие свойства, доступные только для чтения:name (название должности) и salary (зарплата на этой должности). 	
// Задача 18.2: Создайте несколько объектов класса Post: программист, менеджер водитель.
// Задача 18.3: Сделайте класс Employee (работник), в котором будут следующие свойства: name (имя) и surname (фамилия). Пусть начальные значения этих свойств будут передаваться параметром в конструктор. 	
// Задача 18.4: Сделайте геттеры и сеттеры для свойств name и surname. 	
// Задача 18.5: Пусть теперь третьим параметром конструктора будет передаваться должность работника, представляющая собой объект класса Post. Укажите тип этого параметра в явном виде.
// Задача 18.6: Сделайте так, чтобы должность работника (то есть переданный объект с должностью) записывалась в свойство post. 	
// Задача 18.7: Создайте объект класса Employee с должностью программист. При его создании используйте один из объектов класса Post, созданный нами ранее. 	
// Задача 18.8: Выведите на экран имя, фамилию, должность и зарплату созданного работника. 	
// Задача 18.9: Реализуйте в классе Employee метод changePost, который будет изменять должность работника на другую. Метод должен принимать параметром объект класса Post. Укажите в методе тип принимаемого параметра в явном виде. 
var Post = /** @class */ (function () {
    function Post(name, salary) {
        this.name = name;
        this.salary = salary;
    }
    return Post;
}());
var programmer = new Post('programmer', 2000);
var driver = new Post('driver', 1000);
var Employee = /** @class */ (function () {
    function Employee(name, surname, post) {
        this._name = name;
        this._surname = surname;
        this._post = post;
    }
    Employee.prototype.getName = function () {
        return this._name;
    };
    Employee.prototype.setName = function (newName) {
        this._name = newName;
    };
    Employee.prototype.getSurname = function () {
        return this._surname;
    };
    Employee.prototype.setSurname = function (newSurName) {
        this._surname = newSurName;
    };
    Employee.prototype.getPostName = function () {
        return this._post.name;
    };
    Employee.prototype.getPostSalary = function () {
        return this._post.salary;
    };
    Employee.prototype.changePost = function (newPost) {
        this._post = newPost;
    };
    return Employee;
}());
var newEmployer = new Employee('Irina', 'Stoetskaya', programmer);
console.log(newEmployer.getName());
console.log(newEmployer.getSurname());
console.log(newEmployer.getPostName());
console.log(newEmployer.getPostSalary());
newEmployer.changePost(driver);
console.log(newEmployer.getPostName());
