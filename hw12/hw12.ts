// Задача 12.1: Сделайте класс City (город), в котором будут следующие свойства - name (название), foundation (дата основания), population (население). Создайте объект этого класса.
// Задача 12.2: Пусть дана переменная $props, в которой хранится массив названий свойств класса City. Переберите этот массив циклом foreach и выведите на экран значения соответствующих свойств.

class City {
    private name: string;
    private foundation: Date;
    private population: number;

    constructor(name: string, foundation: Date, population: number){
        this.name = name;
        this.foundation = foundation;
        this.population = population;
    }  
}

// won't be anything if we don't create an instance, and try to manipulate with City
const city = new City('Kiev',new Date(2013, 13, 1), 1200)

const $props = Object.keys(city);

$props.forEach((property) => { 
    document.write(`${property}: ${city[property]}`)
});
