// Pадача 6.1: Сделайте класс User, в котором будут следующие свойства - name (имя), age (возраст).	
// Задача 6.2: Сделайте метод setAge, который параметром будет принимать новый возраст пользователя.
// Задача 6.3: Сделайте метод addAge, который параметром будет принимать к-тво лет, которые нужно добавить к возрасту пользователя.
// Задача 6.4: Добавьте также метод subAge, который будет выполнять уменьшение текущего возраста на переданное количество лет.
// Задача 6.5: Сделайте метод, который будет валидировать возраст, название и сам функционал придумайте сами, исходя из опыт написания валидаторов.
// Задача 6.6: Этот метод-валидатор должен вызываться для проверки каждый раз, когда в объект записываются новые данные по возрасту.
// Задача 6.7: Создайте объект этого класса User, проверьте работу методов setAge, addAge и subAge.Числа и примеры придумайте сами. Не забудьте за валидацию возраста.
var User = /** @class */ (function () {
    function User() {
    }
    User.prototype.setAge = function (newAge) {
        if (this.validateAge(newAge)) {
            this.age = newAge;
        }
    };
    User.prototype.addAge = function (count) {
        this.age += count;
    };
    User.prototype.subAge = function (count) {
        this.age = this.age - count;
    };
    User.prototype.validateAge = function (age) {
        return !isNaN(age) && age > 0;
    };
    return User;
}());
var newUser = new User();
newUser.setAge(28);
newUser.setAge('try to use string');
newUser.subAge(2);
newUser.addAge(8);
console.log(newUser);
