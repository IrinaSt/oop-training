// Задача 3.1: Сделайте класс User, в котором будут следующие свойства - name (имя), age (возраст).
// Задача 3.2: Сделайте метод setAge, который параметром будет принимать новый возраст пользователя.
// Задача 3.3: Создайте объект класса User с именем 'Коля' и возрастом 25. С помощью метода setAge поменяйте возраст на 30. Выведите новое значение возраста на экран.
// Задача 3.4: Модифицируйте метод setAge так, чтобы он вначале проверял, что переданный возраст больше или равен 18. Если это так - пусть метод меняет возраст пользователя, а если не так - то ничего не делает.

class User {
  private name: string;
  public age: number;

  constructor(name: string, age: number) {
    this.name = name;
    this.age = age;
  }

  setAge(newAge: number): void {
    if (newAge >= 18) {
      this.age = newAge;
    }
  }
}

const newUser = new User("Коля", 25);
newUser.setAge(30);
document.write(newUser.age);
