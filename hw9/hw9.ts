// Задача 9.1: Сделайте класс Employee, в котором будут следующие private свойства: name (имя), age (возраст) и salary (зарплата).
// Задача 9.2: Сделайте геттеры и сеттеры для всех свойств класса Employee.
// Задача 9.3: Дополните класс Employee приватным методом isAgeCorrect, который будет проверять возраст на корректность (от 1 до 100 лет). Этот метод должен использоваться в сеттере setAge перед установкой нового возраста 	(если возраст не корректный - он не должен меняться).
// Задача 9.4: Пусть зарплата наших работников хранится в долларах. Сделайте так, чтобы 	геттер getSalary добавлял в конец числа с зарплатой значок доллара. То есть еще раз для полной ясности условия задачи:в свойстве salary зарплата будет хранится просто числом, но геттер будет возвращать ее с долларом на конце.

class Employee {
  private name: string;
  private age: number;
  private salary: number;

  public getName(): string {
    return this.name;
  }

  public getAge(): number {
    return this.age;
  }

  public getSalary(): string {
    return `${this.salary} $`;
  }

  public setName(newName: string): void {
    this.name = newName;
  }

  public setAge(newAge: number): void {
    if (this.isAgeCorrect()) {
      this.age = newAge;
    }
  }

  public setSalary(newSalary: number): void {
    this.salary = newSalary;
  }

  private isAgeCorrect(): boolean {
    return this.age >= 1 && this.age <= 100;
  }
}
