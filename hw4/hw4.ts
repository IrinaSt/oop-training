// Задача 4.1: Сделайте класс Employee (работник), в котором будут следующие свойства - name (имя), salary (зарплата). Сделайте метод doubleSalary, который текущую зарплату будет увеличивать в 2 раза.
class Employee {
  private name: string;
  private salary: number;

  doubleSalary(): void {
    this.salary = this.salary * 2;
  }
}
