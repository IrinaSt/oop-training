// Задача 11.1: Сделайте класс Employee, в котором будут следующие публичные свойства - name (имя), age (возраст), salary (зарплата). Сделайте так, чтобы эти свойства заполнялись в методе __construct при создании объекта.	
// Задача 11.2: Создайте объект класса Employee с именем 'Вася', возрастом 25, зарплатой 1000.
// Задача 11.3: Создайте объект класса Employee с именем 'Петя', возрастом 30, зарплатой 2000. 
// Задача 11.4: Выведите на экран сумму зарплат Васи и Пети.

class Employee {
    public name: string;
    public age: number;
    public salary: number;

    constructor(name: string, age: number, salary: number){
        this.name = name;
        this.age = age;
        this.salary = salary;
    }
}

const newUser = new Employee('Вася', 25, 1000);
const newUser1 = new Employee('Петя', 30, 2000);

document.write(newUser.salary + newUser1.salary);