// Задача 14.1: Сделайте класс User, у которого будут приватные свойства surname (фамилия), name (имя) и patronymic (отчество).
// Задача 14.2: Эти свойства должны задаваться с помощью соответствующих сеттеров.
// Задача 14.3: Сделайте так, чтобы эти сеттеры вызывались цепочкой в любом порядке, а самым последним методом в цепочке можно было вызвать метод getFullName, который вернет ФИО юзера (первую букву фамилии, имени и отчества).
// Задача 14.4: Создайте объект и сразу же вызовите у него по цепочке все методы. Повторите в разной последовательности.

class User {
  private surname: string;
  private name: string;
  private patronymic: string;

  constructor(surname: string, name: string, patronymic: string) {
    this.surname = surname;
    this.name = name;
    this.patronymic = patronymic;
  }

  public setSurname(newSurname: string) {
    this.surname = newSurname;
    return this;
  }

  public setName(newName: string) {
    this.name = newName;
    return this;
  }

  public setPatronymic(newPatronymic: string) {
    this.patronymic = newPatronymic;
    return this;
  }

  public getName() {
    return this.name;
  }
}

const newUser = new User("Stoetska", "Irina", "Andriivna");

console.log(newUser.getName());

newUser.setSurname("Khainak").setName("Anton").setSurname("Yurievich");

console.log(newUser.getName());
