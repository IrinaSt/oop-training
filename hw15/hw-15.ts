// Задача 15.1: Сделайте класс Product (товар), в котором будут приватные свойства name (название товара), price (цена за штуку) и quantity. Пусть все эти свойства будут доступны только для чтения.
// Задача 15.2: Добавьте в класс Product метод getCost, который будет находить полную стоимость продукта (сумма умножить на количество).
// Задача 15.3: Сделайте класс Cart (корзина). Данный класс будет хранить список продуктов (объектов класса Product) в виде массива. Пусть продукты хранятся в свойстве products.
// Задача 15.4: Реализуйте в классе Cart метод add для добавления продуктов.
// Задача 15.5: Реализуйте в классе Cart метод remove для удаления продуктов. Метод должен принимать параметром название удаляемого продукта.
// Задача 15.6: Реализуйте в классе Cart метод getTotalCost, который будет 		находить суммарную стоимость продуктов.
// Задача 15.7: Реализуйте в классе Cart метод getTotalQuantity, который будет находить суммарное количество продуктов (то есть сумму свойств quantity всех продуктов).
// Задача 15.8: Реализуйте в классе Cart метод getAvgPrice, который будет находить среднюю стоимость продуктов (суммарная стоимость делить на количество всех продуктов).

class Product {
  readonly name: string;
  readonly price: number;
  readonly quantity: number;

  getCost(): number {
    return this.quantity * this.price;
  }
}

class Cart {
  private products: Product[];
  // private totalCost: number
  public add(product: Product): void {
    this.products.push(product);
  }

  public remove(product: Product): void {
    this.products = this.products.filter(function (element) {
      return element.name !== product.name;
    });
  }

  public getTotalCost(): number {
    return this.products
      .map((element) => element.price)
      .reduce((total, amount) => total + amount);
  }

  public getTotalQuantity(): number {
    return this.products
      .map((element) => element.quantity)
      .reduce((total, amount) => total + amount);
  }

  public getAvgPrice(): number {
    return this.getTotalCost() / this.getTotalQuantity();
  }
}

