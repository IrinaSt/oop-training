// Задача 15.1: Сделайте класс Product (товар), в котором будут приватные свойства name (название товара), price (цена за штуку) и quantity. Пусть все эти свойства будут доступны только для чтения.
// Задача 15.2: Добавьте в класс Product метод getCost, который будет находить полную стоимость продукта (сумма умножить на количество).
// Задача 15.3: Сделайте класс Cart (корзина). Данный класс будет хранить список продуктов (объектов класса Product) в виде массива. Пусть продукты хранятся в свойстве products.
// Задача 15.4: Реализуйте в классе Cart метод add для добавления продуктов.
// Задача 15.5: Реализуйте в классе Cart метод remove для удаления продуктов. Метод должен принимать параметром название удаляемого продукта.
// Задача 15.6: Реализуйте в классе Cart метод getTotalCost, который будет 		находить суммарную стоимость продуктов.
// Задача 15.7: Реализуйте в классе Cart метод getTotalQuantity, который будет находить суммарное количество продуктов (то есть сумму свойств quantity всех продуктов).
// Задача 15.8: Реализуйте в классе Cart метод getAvgPrice, который будет находить среднюю стоимость продуктов (суммарная стоимость делить на количество всех продуктов).
var Product = /** @class */ (function () {
    function Product() {
    }
    Product.prototype.getCost = function () {
        return this.quantity * this.price;
    };
    return Product;
}());
var Cart = /** @class */ (function () {
    function Cart() {
    }
    // private totalCost: number
    Cart.prototype.add = function (product) {
        this.products.push(product);
    };
    Cart.prototype.remove = function (product) {
        this.products = this.products.filter(function (element) {
            return element.name !== product.name;
        });
    };
    Cart.prototype.getTotalCost = function () {
        return this.products
            .map(function (element) { return element.price; })
            .reduce(function (total, amount) { return total + amount; });
    };
    Cart.prototype.getTotalQuantity = function () {
        return this.products
            .map(function (element) { return element.quantity; })
            .reduce(function (total, amount) { return total + amount; });
    };
    Cart.prototype.getAvgPrice = function () {
        return this.getTotalCost() / this.getTotalQuantity();
    };
    return Cart;
}());
