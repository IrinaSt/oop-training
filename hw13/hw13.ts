// Задача 13.1: Есть класс из задачи 2.
// Задача 13.2: Есть ассоциативный массив $methods с ключами method1 и method2:
// <?php 	$methods = ['method1' => 'getName', 'method2' => 'getAge'];
// Задача 13.3: Выведите имя и возраст пользователя с помощью этого массива.
// Задача 13.4: Отдельно вызовите метод getAge сразу после создания объекта.

class Employee {
  private name: string;
  private age: number;
  private salary: number;

  constructor(name: string, age: number, salary: number) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }

  public getName(): string {
    return this.name;
  }

  public getAge(): number {
    return this.age;
  }

  public getSalary(): number {
    return this.salary;
  }

  public checkAge(): boolean {
    return this.age > 18;
  }
}

const methods = {
  method1: "getName",
  method2: "getAge",
};

const employee = new Employee("Ira", 26, 100);

console.log(employee[methods.method1]());
console.log(employee[methods.method2]());

for (let prop in methods) {
  console.log(employee[methods[prop]]());
}

employee.getAge();
