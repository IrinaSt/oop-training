// Задача 10.1: Сделайте класс Employee, в котором будут следующие свойства: name (имя), surname (фамилия) и salary (зарплата).
// Задача 10.2: Сделайте так, чтобы свойства name и surname были доступны только для чтения, а свойство salary - и для чтения, и для записи.

class Employee {
  public readonly name: string;
  public readonly surname: string;
  public salary: number;

  constructor(name: string, surname: string, salary: age) {
    this.name = name;
    this.surname = surname;
    this.salary = salary;
  }
}
