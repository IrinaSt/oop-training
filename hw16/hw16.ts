// Задача 16.1: Сделайте класс Employee с публичными свойствами name (имя) и salary (зарплата).
// Задача 16.2: Сделайте класс Student с публичными свойствами name (имя) и scholarship (стипендия).
// Задача 16.3: Создайте по 3 объекта каждого класса и в произвольном порядке запишите их в массив $arr.
// Задача 16.4: Переберите циклом массив $arr и выведите на экран столбец имен всех работников.
// Задача 16.5: Аналогичным образом выведите на экран столбец имен всех студентов.
// Задача 16.6: Переберите циклом массив $arr и с его помощью найдите сумму зарплат работников и сумму стипендий студентов После цикла выведите эти два числа на экран.

class Employee {
  public name: string;
  public salary: number;

  constructor(name: string, salary: number) {
    this.name = name;
    this.salary = salary;
  }
}

class Student {
  public name: string;
  public scholarship: number;

  constructor(name: string, scholarship: number) {
    this.name = name;
    this.scholarship = scholarship;
  }
}
const $arr = [];

const employee1 = new Employee("Irina", 2000);
$arr.push(employee1);

const employee2 = new Employee("Anna", 1000);
$arr.push(employee2);

const employee3 = new Employee("Inna", 6000);
$arr.push(employee3);

const student1 = new Student("Oleg", 600);
$arr.push(student1);

const student2 = new Student("Ivan", 500);
$arr.push(student2);

const student3 = new Student("Dima", 100);
$arr.push(student3);

$arr
  .filter((element) => element instanceof Employee)
  .forEach((element) => {
    console.log(element.name);
  });

$arr
  .filter((element) => element instanceof Student)
  .forEach((element) => {
    console.log(element.name);
  });

let salarySums = 0;

$arr
  .filter((element) => element instanceof Employee)
  .forEach((element) => {
    salarySums += element.salary;
  });

let scholarShipSums = 0;

$arr
  .filter((element) => element instanceof Student)
  .forEach((element) => {
    scholarShipSums += element.scholarship;
  });

console.log(`Зепка ${salarySums}, стипендии: ${scholarShipSums}`);
