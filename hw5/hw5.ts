// Задача 5.1: Сделайте класс Rectangle (прямоугольник), в котором в свойствах будут записаны ширина и высота (на английском).
// Задача 5.2: В классе Rectangle сделайте метод getSquare, который будет возвращать площадь этого прямоугольника.
// Задача 5.3: В классе Rectangle сделайте метод getPerimeter, который будет возвращать периметр этого прямоугольника.

class Rectangle {
  private width: number;
  private height: number;

  constructor(width: number, height: number) {
    this.width = width;
    this.height = height;
  }

  getSquare(): number {
    return this.width * this.height;
  }
  getPerimeter(): number {
    return (this.width + this.height) * 2;
  }
}
