// Задача 1.1: Сделайте класс Employee (работник), в котором будут следующие свойства - name (имя), age (возраст), salary (зарплата). 	
// Задача 1.2: Создайте объект класса Employee, затем установите его свойства в следующие значения - имя 'Иван', возраст 25, зарплата 1000. 	
// Задача 1.3: Создайте второй объект класса Employee, установите его свойства в следующие значения - имя 'Вася', возраст 26, зарплата 2000. 	
// Задача 1.4: Выведите на экран сумму зарплат Ивана и Васи. 
// Задача 1.5: Выведите на экран сумму возрастов Ивана и Васи.

class Employee {
    public name: string;
    public age: number;
    public salary: number;

    constructor(name: string, age: number, salary: number) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }
}

const firstUser = new Employee('Иван', 25, 1000);
const secondUser = new Employee('Вася', 26, 1000);

document.write(firstUser.salary + secondUser.salary)
document.write(firstUser.age + secondUser.age)