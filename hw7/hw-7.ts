// Задача 7.1 (на приватные и публичные свойства и методы): смените модификатор доступа для вашего валидатора из пункта 6.5 в классе User. Теперь он должен быть не public, а private. 	
// Задача 7.2: Попробуйте вызвать этот метод для валидации возраста снаружи класса. Убедитесь, что это будет вызывать ошибку.



class User {
    public name: string;
    public age: number;

    public setAge(newAge: number): void {
        if(this.validateAge(newAge)) {
            this.age = newAge;   
        }    
    }

    public addAge(count: number): void {
       this.age += count;     
    }

    public subAge(count: number): void {
        this.age = this.age - count;     
     }

    private validateAge(age: number): boolean {
        return !isNaN(age) && age > 0;
    }
}

const newUser = new User();
newUser.validateAge('try to use string');
