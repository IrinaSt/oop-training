// Задача 2.1: Сделайте класс Employee (работник), в котором будут следующие свойства - name (имя), age (возраст), salary (зарплата).
// Задача 2.2: Сделайте в классе Employee метод getName, который будет возвращать имя работника.
// Задача 2.3: Сделайте в классе Employee метод getAge, который будет возвращать возраст работника.
// Задача 2.4: Сделайте в классе Employee метод getSalary, который будет возвращать зарплату работника.
// Задача 2.5: Сделайте в классе Employee метод checkAge, который будет проверять то, что работнику больше 18 лет и возвращать true, если это так, и false, если это не так.
// Задача 2.6: Создайте два объекта класса Employee, запишите в их свойства какие-либо значения. С помощью метода getSalary найдите сумму зарплат созданных работников.

class Employee {
  private name: string;
  private age: number;
  private salary: number;

  public getName(): string {
    return this.name;
  }

  public getAge(): number {
    return this.age;
  }

  public getSalary(): number {
    return this.salary;
  }

  public checkAge(): boolean {
    return this.age > 18;
  }
}

const firstEmployee = new Employee("Irina", 25, 3000);
const secondEmployee = new Employee("Inns", 29, 4000);
const salarySum = firstEmployee.getSalary() + secondEmployee.getSalary();
