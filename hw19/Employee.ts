import User from "./User";

export default class Employee extends User {
  private salary: number;

  constructor(name: string, age: number, salary: number) {
    super(name, age);
    this.salary = salary;
  }

  public setEmployeeLogin() {
    this.setLogin().getLogin() + "Employee" + "salary";
    return this;
  }
}
