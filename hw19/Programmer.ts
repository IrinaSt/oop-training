import Employee from "./Employee";

export default class Programmer extends Employee {
  private langs: Array<string>;

  constructor(name: string, age: number, salary: number, langs: Array<string>) {
    super(name, age, salary);
    this.langs = langs;
  }

  public getLangs(): Array<string> {
    return this.langs;
  }

  public setLangs(newLangs: Array<string>) {
    this.langs = newLangs;
    return this;
  }

  setProgrammerLogin(): void {
    this.setLogin().getLogin() + "Programmer";
  }
}
