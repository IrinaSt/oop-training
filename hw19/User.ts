export default class User {
  private name: string;
  public age: number;
  protected login: string;

  constructor(name: string, age: number) {
    this.name = name;
    this.age = age;
  }

  public setAge(newAge: number): void {
    if (newAge >= 18) {
      this.age = newAge;
    }
  }

  protected setLogin() {
    this.login = this.name.toUpperCase();
    return this;
  }

  protected getLogin(): string {
    return this.login;
  }
}
