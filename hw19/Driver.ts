import Employee from "./Employee";

export default class Driver extends Employee {
  private experience: number;
  private category: "A" | "B" | "C" | "D";
  private driverLogin: string;

  constructor(
    name: string,
    age: number,
    salary: number,
    experience: number,
    category: "A" | "B" | "C" | "D"
  ) {
    super(name, age, salary);
    this.experience = experience;
    this.category = category;
  }

  public getExperience(): number {
    return this.experience;
  }

  public getCategory(): string {
    return this.category;
  }

  public setExperience(newValue: number) {
    this.experience = newValue;
    return this;
  }

  public setCategory(newValue: "A" | "B" | "C" | "D") {
    this.category = newValue;
    return this;
  }

  public setDriverLogin(){
    this.driverLogin = this.setLogin().getLogin() + "Driver";
    return this;
  }
}
